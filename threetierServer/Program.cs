﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using threetierServer.server;
using Ini;

namespace threetierServer
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AllocConsole();
            //Task.Factory.StartNew(Console);
            IniFile ini = new IniFile("C:\\threetier.ini");
            String port = ini.IniReadValue("BASIC", "port");
            String server = ini.IniReadValue("BASIC", "server");
            int portInt;
            DbConnection.Server = server;
            if (int.TryParse(port, out portInt))
            {
                Console.WriteLine("Server started");
                ThreetierController threetierController = new ThreetierController(portInt);
            }
            else
            {
               Console.WriteLine("Wrong port number in threetier.ini"); 
            }

            

        }

        [DllImport("kernel32.dll", EntryPoint = "AllocConsole", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern int AllocConsole();
    }
}
