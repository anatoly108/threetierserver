﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using threetierClient.client;

namespace threetierServer.server
{
    class ThreetierController : IThreetierController 
    {


        public ThreetierController(int port)
        {
            IPAddress ipAddress = Dns.Resolve("localhost").AddressList[0];
            TcpListener tcpListener = null;
            try
            {
                tcpListener = new TcpListener(ipAddress, port);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            tcpListener.Start();

            while (true){
                TcpClient client = tcpListener.AcceptTcpClient();   
                ClientThread clientThread = new ClientThread(client, this);
                clientThread.Start();
            }
        }

        /**
         * Использую перегруженную функцию
         */
        public void NewAppointment()
        {
        }

        public void NewAppointment(XmlDocument appointmentXml)
        {
            String doctor = this.GetValuesFromXml(appointmentXml.OuterXml, "doctor_name")[0].ToString();
            String patient = this.GetValuesFromXml(appointmentXml.OuterXml, "patient_name")[0].ToString();
            String date = this.GetValuesFromXml(appointmentXml.OuterXml, "date")[0].ToString();

            XmlDocument doctorXml = new XmlDocument();
            XmlDocument patientXml = new XmlDocument();

            // Получаем айди по именам
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("doctor");
            xmlWriter.WriteElementString("doctor_name", doctor);
            xmlWriter.WriteEndElement();
            doctorXml.LoadXml(xml.ToString());
            doctorXml = this.GetData(doctorXml);
            String doctorId = this.GetValuesFromXml(doctorXml.OuterXml, "doctor_id")[0].ToString();

            xml = new StringWriter();
            xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("patient");
            xmlWriter.WriteElementString("patient_name", patient);
            xmlWriter.WriteEndElement();
            patientXml.LoadXml(xml.ToString());
            patientXml = this.GetData(patientXml);
            String patientId = this.GetValuesFromXml(patientXml.OuterXml, "patient_id")[0].ToString();

            DbConnection.Query("insert into appointment values (" + doctorId + ", " + patientId + "," + 1 + ",'" + date + "')");
        }

        public void CancelAppointment()
        {
        }

        public void CancelAppointment(XmlDocument appointmentXml)
        {
            String appointmentId = this.GetValuesFromXml(appointmentXml.OuterXml, "visit_id")[0].ToString();
            DbConnection.Query("update appointment set visit_status_id = 3 where visit_id = " + appointmentId);
        }

        public void ResumeAppointment()
        {
        }

        public void ResumeAppointment(XmlDocument appointmentXml)
        {
            String appointmentId = this.GetValuesFromXml(appointmentXml.OuterXml, "visit_id")[0].ToString();
            DbConnection.Query("update appointment set visit_status_id = 1 where visit_id = " + appointmentId);
        }

        public void NewDoctor()
        {
            throw new NotImplementedException();
        }

        public void NewPatient()
        {
            throw new NotImplementedException();
        }

        public void NewDept()
        {
            throw new NotImplementedException();
        }

        public void Search()
        {
            throw new NotImplementedException();
        }

        public void UpdateControls()
        {
            
        }

        /**
         * Выполняет запрос select к базе данных в соответствии с файлом xml
         * Заголовочный элемент должен являться именем таблицы, элементы внутри него - именами столбцов (распознаются как where)
         */
        public XmlDocument GetData(XmlDocument xmlDocument, bool like = false)
        {
            XmlDocument outputXml = new XmlDocument();
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);

            XmlReader reader = XmlReader.Create(new System.IO.StringReader(xmlDocument.OuterXml));
            String table = "";
            // Атрибуты выглядят как атрибут/значение и разделяются в последствии с помощью Split('/')
            ArrayList attributes = new ArrayList();
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    if (reader.IsEmptyElement)
                    {
                        if (table == "")
                            table = reader.Name;
                        //Console.WriteLine("<{0}/>", reader.Name);
                    }
                    else
                    {
                        String attribute = "";
                        if (table == "")
                        {
                            table = reader.Name;
                        }
                        else
                        {
                            //Console.Write("<{0}> ", reader.Name);
                            //reader.Read(); // Read the start tag.
                            //                        if (reader.IsStartElement())
                            //                        {
                            attribute += reader.Name;
                            // Handle nested elements.
                            //Console.Write("\r\n<{0}>", reader.Name);
                            //                        }
                            attribute += "/";
                            attribute += reader.ReadString();
                            attributes.Add(attribute);
                        }

                        //Console.WriteLine(reader.ReadString()); //Read the text content of the element.
                    }
                }
            }

            String where = "";
            String likeOrEquals = "=";
            String percent = ""; // Процент появится при like
            if (like)
            {
                likeOrEquals = "like";
                percent = "%";
            }
            if (attributes.Count != 0)
            {
                where = " where ";
                foreach (string attribute in attributes)
                {
                    String[] attrs = attribute.Split('/');
                    where += attrs[0] + " " + likeOrEquals + " '" + percent + attrs[1] + percent +  "' OR ";
                }
                where = where.Substring(0, where.Length - 3);
            }
            String query = "select * from " + table + where;

            DataSet dataSet = DbConnection.Query(query);
            DataTable dt = new DataTable();
            dt = dataSet.Tables[0];

            // Начинаем создавать xml-документ
            xmlWriter.WriteStartElement(table);
            foreach (DataRow row in dt.Rows)
            {
                xmlWriter.WriteStartElement("entry");
                foreach (DataColumn column in dt.Columns)
                {
                    xmlWriter.WriteElementString(column.ColumnName, row[column].ToString());
                }

                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();

            outputXml.LoadXml(xml.ToString());

            return outputXml;
        }

        private ArrayList GetValuesFromXml(String xmlString, String valueName)
        {
            ArrayList values = new ArrayList();

            XmlReader reader = XmlReader.Create(new System.IO.StringReader(xmlString));
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {

                    if (!reader.IsEmptyElement)
                    {
                        if (reader.Name == valueName)
                        {
                            reader.Read();
                            values.Add(reader.ReadString());
                        }
                    }
                }
            }

            return values;
        }
    }
}
