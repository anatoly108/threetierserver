﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace threetierServer.server
{
    class DbConnection
    {
        private static String server = "";
        private static String _connectionString = @"Data Source="+ server +"; Initial Catalog = threetier; Integrated Security=SSPI;";

        public static DataSet Query(String sqlQuery)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, connection);
            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            sqlQuery = sqlQuery.Replace("\'", "Q");

            String logQuery = "insert into log values ('" + sqlQuery + "')";

            dataAdapter = new SqlDataAdapter(logQuery, connection);
            DataSet dataSet2 = new DataSet();
            dataAdapter.Fill(dataSet2);

            connection.Close();
            return dataSet;
        }

        public static string Server
        {
            get { return server; }
            set { server = value; }
        }
    }
}
