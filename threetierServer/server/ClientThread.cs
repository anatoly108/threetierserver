﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace threetierServer.server
{
    class ClientThread
    {
        private TcpClient tcpClient;
        private ThreetierController threetierController;
        private Thread clientThread;

        public ClientThread(TcpClient tcpClient, ThreetierController threetierController)
        {
            this.tcpClient = tcpClient;
            this.threetierController = threetierController;
            Console.WriteLine("Client connected");
        }

        public void Start()
        {
            clientThread = new Thread(this.WaitingForMessages);    
            clientThread.Start();
        }

        private void WaitingForMessages()
        {
            while (tcpClient.Connected)
            {
                NetworkStream readerStream = tcpClient.GetStream();
                BinaryFormatter outformat = new BinaryFormatter();
                String text = null;
                XmlDocument xmlDocument = new XmlDocument();
                try
                {
                    text = outformat.Deserialize(readerStream).ToString();
                }
                catch (Exception)
                {
                    Console.WriteLine("Client disconnected");
                    break;
                }
                
                // Этот свитч обеспечивает вызов нужных методов. Сердце Заместителя
                switch (text)
                {
                    case "NewAppointment":
                        String xml = outformat.Deserialize(readerStream).ToString();
                        xmlDocument.LoadXml(xml);
                        threetierController.NewAppointment(xmlDocument);
                        break;
                    case "GetData":
                        // Тут будет три строки: GetData, true или false, xml-файл

                        String likeString = outformat.Deserialize(readerStream).ToString();
                        bool like = likeString == "true";

                        // Берём данные, которые отправил клиент
                        String xml2 = outformat.Deserialize(readerStream).ToString();
                        xmlDocument.LoadXml(xml2);
                        // Отправляем их в GetData
                        XmlDocument xmlDocument2 = threetierController.GetData(xmlDocument, like);

                        // Теперь отправляем полученные с помощью GetData данные клиенту
                        NetworkStream writerStream = tcpClient.GetStream();
                        BinaryFormatter format = new BinaryFormatter();
                        format.Serialize(writerStream, xmlDocument2.OuterXml);

                        break;
                    case "CancelAppointment":
                        String appointmentXml = outformat.Deserialize(readerStream).ToString();
                        xmlDocument.LoadXml(appointmentXml);
                        threetierController.CancelAppointment(xmlDocument);
                        break;
                    case "ResumeAppointment":
                        String appointmentXml2 = outformat.Deserialize(readerStream).ToString();
                        xmlDocument.LoadXml(appointmentXml2);
                        threetierController.ResumeAppointment(xmlDocument);
                        break;
                }
            }
            int a = 0;
        }
    }
}
